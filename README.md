# Projet Tuteuré : Traceur GPS pour Sportif Outdoor (Mountrace Project)

## Résumé : 
Notre projet consiste à créer une balise GPS (Mountracer) utilisant le réseau Sigfox qui pourra permettre aux personnes l'utilisant de connaître leurs parcours avec des détails comme le dénivelé, la distance et le temps parcourue.  
Cette balise sera composée d'une carte Arduino Uno, d'un shield mulitprotocole pour pouvoir connecter les shields GPS et Sigfox.

Il est conseillé de se référer au cahier des charges et au rapport situés dans les répertoires du même nom afin de mieux comprendre le fonctionnement de ce projet.


## Pré-requis
- Le prototype traceur GPS (Arduino Uno, Shield multiprotocole, module GPS et module Sigfox + une antenne 868MHz)
- Un Arduino IDE
- Un abonnement Sigfox
- Un accès au backend Sigfox
- Un serveur Web

## Usage
#### Traceur GPS
Assemblez le traceur GPS et récupérez le code GPSwork.ino dans le répertoire Codes_arduino. Copiez le programme dans l'IDE Arduino
et téléversez-le vers votre carte. Assurez vous d'être à l'extérieur dans un endroit dégagé pour recevoir suffisamment de satellites.
Ouvrez le moniteur série dans l'IDE (CTRL+MAJ+M). Vous devriez voir les trames GPS.

#### Module Sigfox
Le code  Sigfox_send_data_in_HEX_to_the_Sigfox_Network.ino dans le répertoire Codes_arduino permet de convertir les données GPS reçues en hexadécimal
afin qu'elle puissent être envoyées sur le réseau Sigfox.

Ce code nécessite des améliorations, c'est pour cela qu'il y a deux codes dissociés.

#### Serveur Web
Ce projet ne propose pas encore de serveur Web fonctionnel pour recevoir les données depuis le réseau Sigfox. Le choix de serveur est donc libre.

## Auteurs
Ce projet a été réalisé par Bryan Adénor et Jean Salaun-Penquer, étudiants à l'IUT de la Réunion.


