/*  
 *  Sigfox Code Example
 *  
 *  Explanation: This example shows how to send a Sigfox packet.
 *  In this example, the message to be sent is defined as a string.
 *  This string defines the diferente HEX values of the bytes to be sent
 *  
 *  Copyright (C) 2015 Libelium Comunicaciones Distribuidas S.L. 
 *  http://www.libelium.com 
 *  
 *  This program is free software: you can redistribute it and/or modify  
 *  it under the terms of the GNU General Public License as published by  
 *  the Free Software Foundation, either version 3 of the License, or  
 *  (at your option) any later version.  
 *   
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of  
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 *  GNU General Public License for more details.  
 *   
 *  You should have received a copy of the GNU General Public License  
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  Version:           1.3
 *  Design:            David Gascon 
 *  Implementation:    Yuri Carmona, Luis Miguel Marti  
 *  Ported to Arduino: Ruben Martin  
 */

#include <Wire.h>

// Cooking API libraries
#include <arduinoUART.h>
#include <arduinoUtils.h>

// Sigfox library
#include <arduinoSigfox.h>

// Pin definition for Sigfox module error LED:
const int error_led =  13;

//////////////////////////////////////////////
uint8_t socket = SOCKET0;     //Asign to UART0
//////////////////////////////////////////////

uint8_t error;


void setup()
{
  pinMode(error_led, OUTPUT);
}


void loop() 
{  

  //////////////////////////////////////////////
  // 1. switch on
  //////////////////////////////////////////////
  error = Sigfox.ON(socket);

  // Check status
  if( error == 0 )
  {
    //"Switch ON OK"
    digitalWrite(error_led, LOW);
  }
  else
  {
    //"Switch ON ERROR"
    digitalWrite(error_led, HIGH);
  }

  //////////////////////////////////////////////
  // 2. send data
  //////////////////////////////////////////////

  char data[] = "Hello world";
  int tailleData = sizeof(data);
  char stock[tailleData];
  String message;
  Serial.println("");
  Serial.println(data);
  for(int i=0;i<tailleData;i++){
    sprintf(stock,"%2X",data[i]);
    message += stock;
    //Serial.println(data[i]);
  }
  Serial.println(message);
  message.toCharArray(stock,tailleData);
  // Send 12 bytes at most
  error = Sigfox.send((char *) stock);

  // Check sending status
  if( error == 0 )
  {
    //"Sigfox packet sent OK"
    digitalWrite(error_led, LOW);
  }
  else
  {
    //"Sigfox packet sent ERROR"
    digitalWrite(error_led, HIGH);
  }

  //////////////////////////////////////////////
  // 3. sleep
  //////////////////////////////////////////////
  delay(60000);
}
