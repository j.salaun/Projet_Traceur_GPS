//Librairie pour permettre de créer une liaison série
#include <SoftwareSerial.h>
//Librairies pour le shield multiprotocol (important !)
#include <MCP23008.h>
#include <multiprotocolShield.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(0, 1); // On déclare notre liaison série avec comme paramettre (RX, TX)

int i = 0;
int taille = 0;
String msg = "";
char str[50];

//Variable pour la lat, long, et le time
String latitude, longitude, hour, minute, second;

void setup() {
  //Configuration 
  Serial.begin(9600); //La liason pour afficher dans le moniteur série
  mySerial.begin(9600); //Configuration de la liaison série entre la carte arduino UNO et le module GPS (ça permet d'envoyer des requêtes au module GPS)
  socket1.ON(); //Activation de la socket 1
  delay(100);
  socket1.setCS();  //Enables the SPI of the SOCKET1 (5V Level)
  delay(100);
  socket1.setMUX(); //Configures the multiplexor in the SOCKET1
  Serial.println("Configuration terminee !");
  
  Serial.println("Envoie de la requete pour rechercher des satellites, en cours...");
  //mySerial.print("PMTK314,1,1,1,1,1,5,0,0,0,0,0,0,0,0,0,0,0,1,0*2D<CR><LF>");  //Envoie de la requete pour chercher les satellites GPS
  delay(2000);
   
}

void loop() {
  //Permet de lire les trames NMEA sur le moniteur série 
  if (mySerial.available()) {
    
   char resultat = mySerial.read();
   Serial.print(resultat);
    
   //Manipulation de la trame
   if(resultat != '$')
    {
      //Serial.println("----------Debut du message !------------");
      if (resultat != " ") {
        str[i] = resultat;
        /*Serial.print(" STR1 : ");
        Serial.print(i);
        Serial.print(" : ");
        Serial.println(str[i]);*/
        i++;
      }
    }
    //Prochaine trame NMEA
    if(resultat == '$')
      {
        //String testChar = String(str[4]);
        //Serial.println(testChar);
        //Serial.println(str[3]);          
        //Serial.println(str[4]);
        if (String(str[3]) == "G" && String(str[4]) == "A"){ //Filtre les trames pour ne laisser que passer les trame GPGGA
          //Serial.println(str[3]);          
          //Serial.println(str[4]);
          //Serial.print("STR2 : ");
          //Serial.println("Ca marche !");
          for (int j = 0; j<=i; j++){
            if (str[j] != " "){
              msg += str[j];
            }
          }
        }
        
          //Faire fonction pour récupérer la date de l'envoie du message
          //Time
          hour = String(str[6]) + String(str[7]);
          minute = String(str[8]) + String(str[9]);
          second = String(str[10]) + String(str[11]);
          Serial.print("Heure : ");
          Serial.print(hour);
          Serial.print(":");
          Serial.print(minute);
          Serial.print(":");
          Serial.println(second);
          //Faire la fonction pour récupérer la Latitude et la Longitude
          //Faire condition si c'est vide dire "pas de coordonnée"
          //Latitude
          latitude = String(str[17]) + String(str[18]) + String(str[19]) + String(str[20]) + String(str[21]) + String(str[22]) + String(str[23]) + String(str[24]) + String(str[25]) + String(str[27]);
          //Longitude
          longitude = String(str[30]) + String(str[31]) + String(str[32]) + String(str[33]) + String(str[34]) + String(str[35]) + String(str[36]) + String(str[37]) + String(str[38]) + String(str[40]);
          Serial.print("Longitude : ");
          Serial.println(longitude);
          Serial.print("Latitude : ");
          Serial.println(latitude);
        
        //Afficher le message MSG
        Serial.print("MSG : ");
        Serial.print(msg);
        Serial.println("");
        //Vider MSG pour stocker les autres messages
        msg = "";
        for (int k = 0; k<=i; k++){
          str[k] = {0};
          }
        i = 0;
        //Serial.println("***********Fin du message !**************");
        Serial.println();
        Serial.println();
      }
  }
}
